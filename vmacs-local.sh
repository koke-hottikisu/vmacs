#!/bin/bash

BASEDIR=$(cd $(/usr/bin/dirname $0); pwd)

SRC=$BASEDIR/vmacs.sh
DEST=$BASEDIR/tmp-shell-for-vmacs-local.sh

cp $SRC $DEST
chmod +x $DEST

sed -i -e "s@docker pull registry.gitlab.com/koke-hottikisu/vmacs@docker build -t vmacs $BASEDIR@g" $DEST
sed -i -e "s@registry.gitlab.com/koke-hottikisu/vmacs@vmacs@g" $DEST

$DEST
