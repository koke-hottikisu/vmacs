(require 'package)
(setq package-user-dir "/.vmacs.d/elpa")

(package-initialize)
(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
    ("melpa" . "http://melpa.org/packages/")
    ("org" . "http://orgmode.org/elpa/")))

(add-to-list 'load-path "/.vmacs.d/site-lisp")

(unless package-archive-contents
  (package-refresh-contents))
(when (not (package-installed-p 'use-package))
  (package-install 'use-package))
(require 'use-package)

(use-package init-loader
  :ensure t
  :config
  (init-loader-load "/.vmacs.d/conf"))
