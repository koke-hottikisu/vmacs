(use-package beacon
  :ensure t
  :custom
  (beacon-color "gray")
  (beacon-blink-when-focused t)
  :config
  (beacon-mode 1))
