(use-package exec-path-from-shell
  :ensure t)

(use-package go-mode
  :ensure t
  :commands go-mode
  :hook (go-mode . lsp))
(add-hook 'go-mode-hook (lambda () (setq tab-width 2)))
(exec-path-from-shell-copy-envs '("PATH" "GOROOT" "GOPATH"))
