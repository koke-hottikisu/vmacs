(use-package hydra
  :ensure t)

(use-package ivy
  :ensure t
  :custom
  (ivy-use-virtual-buffers t)
  (enable-recursive-minibuffers t)
  :config (ivy-mode 1))

(use-package ivy-rich
  :ensure t
  :after (counsel)
  :config (ivy-rich-mode 1))

(use-package counsel
  :ensure t
  :custom
  (counsel-find-file-ignore-regexp (regexp-opt '("~")))
  :bind
  ("M-x" . counsel-M-x)
  ("C-x d" . counsel-find-file)
  ("C-x C-f" . counsel-find-file))


(use-package swiper
  :ensure t
  :bind
  (("C-s" . swiper)
   ("C-r" . swiper)))
