(add-to-list 'load-path "/usr/share/emacs/site-lisp/emacs-mozc")

(use-package mozc
  :ensure t
  :custom
  (default-input-method "japanese-mozc")
  :bind
  ("C-;" . toggle-input-method))
