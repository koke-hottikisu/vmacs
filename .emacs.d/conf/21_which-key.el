(use-package which-key
  :ensure t
  :init (which-key-mode)
  :diminish which-key-mode))
