(use-package whitespace
  :ensure t
  :config
  (whitespace-mode 1))
