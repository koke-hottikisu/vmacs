(use-package eww
  :ensure t
  :commands eww eww-follow-link
  :init
  (setq browse-url-browser-function 'eww-browse-url)
  (setq eww-search-prefix "http://www.google.com/search?q=")
  :config
  (add-hook 'eww-after-render-hook 'ha/eww-rerender-pages)
  (add-hook 'eww-mode 'ace-link-mode)
  :bind (("C-c w w" . eww)
         ("C-c w l" . eww-follow-link)))
