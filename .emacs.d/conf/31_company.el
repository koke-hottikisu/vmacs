(use-package company
  :ensure t
  :init (global-company-mode)
  :bind
  (:map company-active-map
	("C-n" . company-select-next)
	("C-p" . company-select-previous))
  (:map company-search-map
	("C-p" . company-select-previous)
	("C-n" . company-select-next))
  :config
  (setq company-idle-delay 0)
  (setq company-show-numbers nil)
  (setq company-minimum-prefix-length 1)
  (setq company-selection-wrap-around t)
  (add-to-list 'company-backends 'company-edbi)
  (global-set-key (kbd "C-<tab>") 'company-complete))

(use-package company-box
  :ensure t
  :hook (company-mode . company-box-mode))
