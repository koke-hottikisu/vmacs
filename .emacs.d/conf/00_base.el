;; 対応する括弧を光らせる
(show-paren-mode 1)

;; "yes or no" の選択を "y or n" にする
(fset 'yes-or-no-p 'y-or-n-p)

;; disable bar
(scroll-bar-mode 0)
(menu-bar-mode 0)
(tool-bar-mode 0)

;; enable display time
(display-time-mode 1)

;; use icon
(use-package all-the-icons
  :ensure t
  :init (if (not (file-exists-p "~/.local/share/fonts/all-the-icons.ttf"))
	    (all-the-icons-install-fonts t)))

;; tab-bar-mode
(tab-bar-mode 1)

;; enable trash
(custom-set-variables
 '(delete-by-moving-to-trash t)
 '(trash-directory "~/.Trash"))
