(use-package neotree
  :ensure t
  :bind (("C-x f" . neotree-toggle))
  :custom (neo-show-hidden-files t))
