(define-key key-translation-map (kbd "C-h") (kbd "<DEL>"))

(global-set-key (kbd "C-?") 'help-for-help) 

(global-set-key (kbd "C-x p") 'previous-multiframe-window)
