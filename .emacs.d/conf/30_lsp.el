(use-package lsp-mode
  :ensure t
  :hook
  (python-mode . lsp)
  (shell-script-mode . lsp)
  :custom
  (lsp-session-file "/.vmacs.d/.lsp-session-v1"))

(use-package lsp-ui
  :ensure t
  :bind
  (:map lsp-mode-map
	("C-c C-r" . lsp-ui-peek-find-references)
	("C-c C-j" . lsp-ui-peek-find-definitions)))

(use-package popup
  :ensure t)

(use-package flymake-diagnostic-at-point
  :after flymake
  :config
  (add-hook 'flymake-mode-hook #'flymake-diagnostic-at-point-mode))
