#!/bin/bash

usermod -u $HOST_USER_ID -l $HOST_USER_NAME -md $HOST_HOME -p pass vmacs-tmp
groupmod -g $HOST_GROUP_ID -n $HOST_GROUP_NAME vmacs-tmp

chown -R $HOST_USER_ID:$HOST_GROUP_ID $VMACS_D
chown -R $HOST_USER_ID:$HOST_GROUP_ID $HOST_HOME/.local/share/fonts/
chown -R $HOST_USER_ID:$HOST_GROUP_ID $HOST_HOME/.mozc
chown -R $HOST_USER_ID:$HOST_GROUP_ID $HOST_HOME/.dbus

su - $HOST_USER_NAME -c "PATH=$GOPATH/bin:/usr/local/go/bin:$PATH GOPATH=$GOPATH emacs -q -l $VMACS_D/init.el"
