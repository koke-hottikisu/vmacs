FROM ubuntu:18.04 AS builder

# install packages for build emacs
RUN apt-get update && \
	DEBIAN_FRONTEND="noninteractive" TZ="Asia/Tokyo" apt-get install -y \
	wget \
	make \
	gcc \
	libgnutls28-dev \
	libncurses5-dev \
	libjpeg-dev libpng-dev libgif-dev libtiff-dev \
	libx11-dev libxpm-dev \
	libgtk-3-dev libwebkit2gtk-4.0-dev

RUN wget http://ftp.gnu.org/gnu/emacs/emacs-27.1.tar.gz && \
	tar -xzf emacs-27.1.tar.gz && \
	rm emacs-27.1.tar.gz

# build emacs
WORKDIR /emacs-27.1
RUN ./configure --with-xwidgets && \
	make && \
	make install

FROM ubuntu:18.04

COPY --from=builder /usr/local/bin/emacs /usr/local/bin/emacs
COPY --from=builder /usr/local/libexec/emacs/27.1/x86_64-pc-linux-gnu/ /usr/local/libexec/emacs/27.1/x86_64-pc-linux-gnu/
COPY --from=builder /usr/local/share/emacs/27.1/etc/ /usr/local/share/emacs/27.1/etc/
COPY --from=builder /usr/local/share/emacs/27.1/lisp /usr/local/share/emacs/27.1/lisp
COPY --from=builder /usr/local/share/emacs/27.1/etc/charsets /usr/local/share/emacs/27.1/etc/charsets

RUN apt-get update && \
	DEBIAN_FRONTEND=noninteractive \
	TZ=Asia/Tokyo \
	apt-get install -y \
	libgif7 \
	libxpm4 libsm6 libice6 libxft2 \
	libgtk-3-0 libwebkit2gtk-4.0 \
	fonts-ricty-diminished \
	emacs-mozc emacs-mozc-bin markdown \
	tzdata language-pack-ja \
	git \
	curl gzip \
	npm \
        python3-venv python3 python3-pip && \
	rm -rf /var/lib/apt/lists/*
RUN cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
RUN update-locale LANG=ja_JP.UTF-8

RUN curl -LO https://golang.org/dl/go1.15.8.linux-amd64.tar.gz && \
	tar -C /usr/local -xzf go1.15.8.linux-amd64.tar.gz

RUN pip3 install -U python-language-server setuptools

RUN npm install -g n
RUN n latest
RUN npm update -g npm
RUN npm install -g typescript-language-server

# plantuml
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    TZ=Asia/Tokyo \
    apt-get install -y default-jre \
    graphviz && \
    rm -rf /var/lib/apt/lists/*
RUN curl -o /opt/plantuml.jar https://jaist.dl.sourceforge.net/project/plantuml/plantuml.jar

# setup os
RUN useradd -G sudo --create-home vmacs-tmp
ENV HOME /home/vmacs-tmp
WORKDIR /home/vmacs-tmp

ENV VMACS_D /.vmacs.d
RUN mkdir -p $VMACS_D/elpa

COPY .emacs.d/conf $VMACS_D/conf
COPY .emacs.d/site-lisp $VMACS_D/site-lisp
COPY .emacs.d/init.el $VMACS_D/init.el

COPY entrypoint.sh /entrypoint.sh
CMD bash /entrypoint.sh
