#!/bin/bash
xhost +local:
docker pull registry.gitlab.com/koke-hottikisu/vmacs
docker run -it --rm \
       -v /tmp/.X11-unix:/tmp/.X11-unix \
       -v $HOME:$HOME \
       -v $HOME/.mozc/ \
       -v vmacs-elpa:/.vmacs.d/elpa \
       -v vmacs-fonts:$HOME/.local/share/fonts \
       -e DISPLAY=$DISPLAY \
       -e HOST_USER_ID=`id -u` \
       -e HOST_USER_NAME=`id -un` \
       -e HOST_GROUP_ID=`id -g` \
       -e HOST_GROUP_NAME=`id -gn` \
       -e HOST_HOME=$HOME \
       -e GOPATH=$GOPATH \
       registry.gitlab.com/koke-hottikisu/vmacs
xhost -local:
